# NodeJS Library

## Installation

`git clone https://gitlab.com/Astroop/nodejs-library.git`

La logique back-end et front-end du projet sont distincts, ce qui signifie qu'il va falloir lancer 2 serveurs.

 1.  Commencez par ouvrir un terminal puis positionnez vous dans le répertoire 'client/'
 2.  Installez les différentes dépendances

`npm install`

 3. Une fois les dépendances installées, vous pouvez lancer le serveur front à l'aide de la commande suivante :

`npm run serve`

Cela aura pour effet de créer un serveur à l'url indiquée (localhost:8080)

  4. Ouvrez ensuite un second terminal puis positionnez vous dans le répertoire 'server/', ce sera notre API
  5. Installez les différentes dépendances à l'aide de npm
  6. Puis lancez le serveur à l'aide de la commande suivante :

`npm start`

Parfait ! Nos 2 serveurs peuvent communiquer, rendez-vous désormais à l'url indiquée côté client
[localhost:8080](localhost:8080/login)

## Connexion

Il n'y pas de BDD à configurer, le projet disposent déjà de 2 utilisateurs, un admin et un abonné

### Identifiants 

Admin :       

*  admin@admin.com
* 123456 

Utilisateur : 

*  user@user.com
*  123456

## Fonctionnalités

Les 2 utilisateurs disposent de différents droits chacun. 
Ainsi avec le compte administrateur vous pourrez vous rendre sur les pages de gestion et de modifications, tandis qu'avec le compte utilisateur vous pourrez consulter et emprunter des ouvrages.

