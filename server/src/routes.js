const AuthenticationController = require('./controllers/AuthenticationController')
const BookController = require('./controllers/BookController')
const LoanController = require('./controllers/LoanController')
const UserController = require('./controllers/UserController')

const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')

module.exports = (app) => {
  app.post('/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register
  )
  app.post('/login',
    AuthenticationController.login
  )
  app.get('/users',
    UserController.index
  )
  app.post('/users',
    UserController.store
  )
  app.get('/books',
    BookController.index
  )
  app.post('/books',
    BookController.store
  )
  app.get('/books/:bookId',
    BookController.show
  )
  app.put('/books/:bookId',
    BookController.update
  )
  app.get('/users/:userId',
    UserController.show
  )
  app.put('/users/:userId',
    UserController.update
  )
  app.delete('/books/:bookId',
    BookController.destroy
  )
  app.delete('/users/:userId',
    UserController.destroy
  )
  app.get('/loans',
    LoanController.index
  )
  app.get('/loans/:bookId',
    LoanController.isLoan
  )

  app.post('/loans',
    LoanController.loan
  )
  app.get('/users/:userId/loans',
    LoanController.myBooks
  )

  app.delete('/loans/:loanId',
    LoanController.returnBook
  )
}
