const { Loan, Book, User } = require('../models')
const _ = require('lodash')

module.exports = {
  async index (req, res) {
    try {
      const loans = await Loan.findAll({
        include: [
          {
            model: Book
          },
          {
            model: User
          }
        ]
      })
      res.send(loans)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async loanBy (req, res) {
    try {
      const { bookId } = req.query
      const loan = await Loan.findOne({
        where: {
          BookId: bookId
        }
      })
      res.send(loan)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async isLoan (req, res) {
    try {
      const userId = req.query.userId
      const bookId = req.params.bookId
      const loan = await Loan.findOne({
        where: {
          BookId: bookId,
          UserId: userId
        }
      })
      res.send(loan)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async loan (req, res) {
    try {
      const loan = await Loan.create(req.body)
      res.send(loan)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async returnBook (req, res) {
    try {
      const { loanId } = req.params
      const loan = await Loan.findByPk(loanId)
      await loan.destroy()
      res.send(loan)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async myBooks (req, res) {
    try {
      const loans = await Loan.findAll({
        where: {
          UserId: req.params.userId
        },
        include: [
          {
            model: Book
          }
        ]
      })
        .map(loan => loan.toJSON())
        .map(loan => _.extend({
          loanId: loan.id,
          loanCreation: loan.createdAt
        }, loan.Book))
      res.send(loans)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  }
}
