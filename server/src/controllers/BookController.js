const { Book } = require('../models')

module.exports = {
  async index (req, res) {
    try {
      const books = await Book.findAll()
      res.send(books)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async store (req, res) {
    try {
      const book = await Book.create(req.body)
      res.send(book)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async show (req, res) {
    try {
      const book = await Book.findByPk(req.params.bookId)
      res.send(book)
    } catch (err) {
      res.status(404).send({
        error: 'An error has occured'
      })
    }
  },
  async update (req, res) {
    try {
      const book = await Book.update(req.body, {
        where: {
          id: req.params.bookId
        }
      })
      res.send(book)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async destroy (req, res) {
    try {
      const { bookId } = req.params
      const book = await Book.findByPk(bookId)
      await book.destroy()
      res.send(book)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  }
}
