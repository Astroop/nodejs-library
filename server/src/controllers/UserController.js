const { User } = require('../models')

module.exports = {
  async index (req, res) {
    try {
      const users = await User.findAll()
      res.send(users)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async store (req, res) {
    try {
      const user = await User.create(req.body)
      res.send(user)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async show (req, res) {
    try {
      const user = await User.findByPk(req.params.userId)
      console.log(user)
      res.send(user)
    } catch (err) {
      res.status(404).send({
        error: 'An error has occured'
      })
    }
  },
  async update (req, res) {
    try {
      const user = await User.update(req.body, {
        where: {
          id: req.params.userId
        }
      })
      res.send(user)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  },
  async destroy (req, res) {
    try {
      const { userId } = req.params
      const user = await User.findByPk(userId)
      await user.destroy()
      res.send(user)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured'
      })
    }
  }
}
