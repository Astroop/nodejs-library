module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define('Book', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    author: {
      type: DataTypes.STRING,
      allowNull: false
    },
    resume: DataTypes.TEXT,
    editeur: DataTypes.STRING,
    nb_pages: DataTypes.INTEGER
  })

  return Book
}
