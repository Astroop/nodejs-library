module.exports = (sequelize, DataTypes) => {
  const Loan = sequelize.define('Loan', {})

  Loan.associate = function (models) {
    Loan.belongsTo(models.User)
    Loan.belongsTo(models.Book)
  }

  return Loan
}
