const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcryptjs'))

async function hashPassword (user, options) {
  const SALT_FACTOR = 8

  if (!user.changed('password')) {
    return
  }

  const result = await bcrypt.hash(user.password, SALT_FACTOR).then(function (hash) {
    return hash
  })

  user.setDataValue('password', result)
}

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    firstname: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    genre: {
      type: DataTypes.CHAR,
      max: 1
    },
    tel: DataTypes.INTEGER,
    address: DataTypes.STRING,
    is_admin: DataTypes.BOOLEAN
  },
  {
    hooks: {
      beforeSave: hashPassword
    }
  })

  User.prototype.comparePassword = function (password) {
    return bcrypt.compare(password, this.password)
  }

  return User
}
