const Joi = require('joi')

module.exports = {
  register (req, res, next) {
    const schema = {
      email: Joi.string().email(),
      password: Joi.string().regex(
        new RegExp('^[a-zA-Z0-9]{6,32}$')
      ),
      firstname: Joi.string(),
      lastname: Joi.string(),
      genre: Joi.string().max(1),
      is_admin: Joi.boolean(),
      address: Joi.string(),
      tel: Joi.number().integer()
    }

    const { error } = Joi.validate(req.body, schema)

    if (error) {
      switch (error.details[0].context.key) {
        case 'email':
          res.status(400).send({
            error: 'You must provide a valid email address'
          })
          break
        case 'password':
          res.status(400).send({
            error: 'Password must be at least 6 characters in length'
          })
          break
        default:
          res.status(400).send({
            error: 'Invalid registration informations'
          })
          break
      }
    } else {
      next()
    }
  }
}
