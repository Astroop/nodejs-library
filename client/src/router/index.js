import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Register from '../views/Register.vue'
import Login from '../views/Login.vue'
import indexBooks from '../views/books/Index.vue'
import indexUsers from '../views/users/Index.vue'
import indexLoans from '../views/loans/Index.vue'
import showBook from '../views/books/Show.vue'
import editBook from '../views/books/Edit.vue'
import editUser from '../views/users/Edit.vue'
import userBooks from '../views/users/Loans.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/books',
    name: 'index.books',
    component: indexBooks
  },
  {
    path: '/books/:bookId',
    name: 'show.books',
    component: showBook
  },
  {
    path: '/books/:bookId/edit',
    name: 'edit.books',
    component: editBook
  },
  {
    path: '/users/:userId/edit',
    name: 'edit.users',
    component: editUser
  },

  {
    path: '/users',
    name: 'index.users',
    component: indexUsers
  },
  {
    path: '/profile/books',
    name: 'user.books',
    component: userBooks
  },
  {
    path: '/loans',
    name: 'index.loans',
    component: indexLoans
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
