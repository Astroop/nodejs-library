import Api from '@/services/Api'

export default {
    index () {
        return Api().get('books')
    },
    store (book) {
        return Api().post('books', book)
    },
    show (bookId) {
        return Api().get(`books/${bookId}`)
    },
    update (book) {
        return Api().put(`books/${book.id}`, book)
    },
    destroy (bookId) {
        return Api().delete(`/books/${bookId}`)
    }
}
