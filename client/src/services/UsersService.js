import Api from '@/services/Api'

export default {
  index () {
    return Api().get('users')
  },
  store (user) {
    return Api().post('users', user)
  },
  show (userId) {
    return Api().get(`users/${userId}`)
  },
  update (user) {
      return Api().put(`users/${user.id}`, user)
  },
  destroy (userId) {
    return Api().delete(`/users/${userId}`)
  }
}
