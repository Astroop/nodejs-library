import Api from '@/services/Api'

export default {
  index () {
    return Api().get('loans')
  },
  isLoan ({userId, bookId}) {
    return Api().get(`loans/${bookId}`, {
			params: {
				userId: userId
			}
		})
	},
	loan (loan) {
    	return Api().post('/loans', loan)
	},
	returnBook (loanId) {
    	return Api().delete(`loans/${loanId}`)
	},
	user (userId) {
		return Api().get(`users/${userId}/loans`)
	}
}
