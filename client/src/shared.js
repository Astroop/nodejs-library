export default {
  validateForm: function(form) {
      var y, i, e, valid = true;
      e =  document.getElementsByClassName('error-message');
      y = form.getElementsByTagName("input");
      for (i = 0; i < y.length; i++) {
        if (y[i].getAttribute('required') == "true" || y[i].getAttribute('required') == "required" && y[i].value == "") {
          y[i].className += " uk-form-danger";
          e[i].innerHTML = 'Vous ne pouvez pas laisser ce champ vide.';
          valid = false;
        }
      }
      return valid;
  }
}