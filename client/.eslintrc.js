module.exports = {
    env: {
      browser: true,
      es6: true
    },
    extends: [
      'plugin:vue/essential'
    ],
    rules: {
        'no-console': 'off',
    },
  }
  