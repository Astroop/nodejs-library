let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
mix.autoload({
	jquery: ['$', 'window.jQuery']
});

mix.styles([
	'./node_modules/uikit/dist/css/uikit.css',
	'./src/assets/css/notyf.min.css',
	'./src/assets/css/style.css'
], './public/css/app.css');
